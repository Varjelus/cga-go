package cga

// RFC 3972

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"hash"
	"math/big"
	"net"
)

const MAXSEC byte = 0x07

var (
	ErrSecurityLimit     = errors.New(`security argument not within parameters (0-7)`)
	ErrInvalidIP         = errors.New(`provided argument is not a valid IP address`)
	ErrTooManyCollisions = errors.New(`too many collisions`)
	ErrDomainMismatch    = errors.New(`address subnet prefix mismatch`)
	ErrAnonymous         = errors.New(`address is anonymous`) // Contains only a subnet prefix.
	ErrUnverified        = errors.New(`unverified`)
	ErrNotSecure         = errors.New(`failed security check`)
)

type ColliderFunc func([]byte) bool

type CGA struct {
	Address      []byte `json:"CGA"` // Bytes [0:8] = subnet prefix
	Modifier     []byte `json:"modifier"`
	Identity     []byte `json:"identity"`
	SubnetPrefix []byte `json:"subnetPrefix,omitempty"`
	Collisions   byte   `json:"collCount,omitempty"`
	PublicKey    []byte `json:"publicKey"`
	ExtFields    []byte `json:"extFields,omitempty"`
	Hash         []byte `json:"hash,omitempty"` // Undefined structure. Assume SHA-1 if nil.
}

func Generate(ip net.IP, sec byte, publicKey []byte, extFields []byte, collider ColliderFunc) (*CGA, error) {
	// Convert IPv4 addresses to IPv6 format.
	ip = ip.To16()
	if ip == nil {
		return nil, ErrInvalidIP
	}

	// Generate CGA
	return Custom(ip[:8], sec, publicKey, extFields, collider, sha1.New())
}

// GenerateCustom generates a CGA-esque, proof-of-work fingerprint for arbitrary data.
func Custom(domain []byte, sec byte, publicKey []byte, extFields []byte, collider ColliderFunc, h hash.Hash) (*CGA, error) {
	// Check security parameter.
	if sec > MAXSEC {
		return nil, ErrSecurityLimit
	}

	var (
		cga *CGA = &CGA{}
		err error
	)
	cga.SubnetPrefix = domain
	cga.Identity, cga.Modifier, cga.Collisions, err = identity(sec, cga.SubnetPrefix, publicKey, extFields, collider, h)
	if err != nil {
		return nil, err
	}
	cga.PublicKey = publicKey
	cga.ExtFields = extFields
	cga.Address = append(cga.SubnetPrefix, cga.Identity...)

	return cga, nil
}

func identity(sec byte, prefix []byte, publicKey []byte, extFields []byte, collider ColliderFunc, h hash.Hash) (id []byte, mod []byte, collCount byte, err error) {
	if h == nil {
		h = sha1.New() // This is the CGA default.
	}

	// Initialize modifier with 16 octets (128 bits) of random.
	modifier := big.NewInt(0)
	randLimit := big.NewInt(0).SetBytes(bytes.Repeat([]byte{0xff}, 16)) // 16 octets (128 bits) of 0xff.
	modifier, err = rand.Int(rand.Reader, randLimit)
	if err != nil {
		return
	}
	// Initialize digest.
	digest := make([]byte, h.Size())
	defer h.Reset()
	// Initialize misc.
	one := big.NewInt(1) // We will need this many times.
	keyFields := append(publicKey, extFields...)
	paddedKeyFields := append(make([]byte, 9), keyFields...)
	secBytes := make([]byte, 2*sec)
	// Brute-force loop.
	for {
		// Concatenate the 16 octets of random data, 9 zero octets, the public key and extra fields. Then hash it.
		digest = h.Sum(modifier.Append(paddedKeyFields, 16))
		// Check that we have enough leading zeroes.
		if sec != 0 && bytes.Compare(digest[:2*sec], secBytes) != 0 {
			modifier.Add(modifier, one)
			continue
		}
		break
	}
	// Collision avoidance loop.
	for {
		// Append modifier, prefix, collision count, public key and extFields together.
		digest = h.Sum(modifier.Append(append(
			append(prefix, collCount),
			keyFields...,
		), 16))
		id = digest[:8]                 // First 8 octets (64 bits) of hash.
		id[0] = id[0]&0x1c | (sec << 5) // Write sec and u/g bits.
		id = append(prefix, id...)

		if collider != nil && collider(id) {
			collCount = collCount + 1
			if collCount > 2 {
				err = ErrTooManyCollisions
				return
			}
			continue
		}
		break
	}

	mod = modifier.Bytes()
	return
}

func (a *CGA) Verify(h hash.Hash) error {
	if a.Collisions > 2 {
		return ErrTooManyCollisions
	}
	if len(a.Address) < len(a.SubnetPrefix) || bytes.Compare(a.Address[:len(a.SubnetPrefix)], a.SubnetPrefix) != 0 {
		return ErrDomainMismatch
	}

	if h == nil {
		h = sha1.New()
	}

	// Append modifier, prefix, collision count, public key and extra fields together.
	keyFields := append(a.PublicKey, a.ExtFields...)
	digest := h.Sum(append(
		a.Modifier,
		append(
			append(a.SubnetPrefix, a.Collisions),
			keyFields...,
		)...,
	))

	prefix := digest[:len(a.SubnetPrefix)] // Domain.
	prefix[0] = prefix[0] & 0x1c           // Ignore sec and u/g bits.

	// Check that we have some other data than the domain.
	if len(digest) <= len(a.SubnetPrefix) {
		return ErrAnonymous
	}

	id := a.Address[len(a.SubnetPrefix):] // Identifier.
	id[0] = id[0] & 0x1c                  // Ignore sec and u/g bits.

	if bytes.Compare(prefix, id) != 0 {
		return ErrUnverified
	}

	// Concatenate the modifier, 9 zero octets, the public key and extra fields. Then hash it.
	digest = h.Sum(append(
		a.Modifier,
		append(
			make([]byte, 9),
			keyFields...,
		)...,
	))

	// Extract sec from identifier.
	sec := a.Address[len(a.SubnetPrefix)] >> 5
	// Check that we got enough leading zeroes.
	if sec != 0 && bytes.Compare(digest[:2*sec], make([]byte, 2*sec)) != 0 {
		return ErrNotSecure
	}

	return nil
}

func (a *CGA) String() string {
	return hex.EncodeToString(a.Address)
}
